### Installation
```
npm install --save cc-style
```
### Usage
```
//常用的样式
import 'node_modules/cc-style/dist/common.min.css'
//规范样式表
import 'node_modules/cc-style/dist/normalize.min.css'
```
对所有样式均采用统一命名方法".品牌名-属性名首写字母联合不保留破折号-属性值首写字母联合保留破折号"，比如品牌名称设置为`cc`,需要调用`justify-content: flex-start`属性，则合成类名为`cc-jc-f-s`
#### common.min.css
由以下目录结构文件内容组成
style
├── color.scss
├── flex.scss
├── font.scss
├── margin.scss
├── padding.scss
└── prefix.scss

#### normalize.min.css
style
└── normalize.scss