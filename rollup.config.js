import scss from 'rollup-plugin-scss';
import postcss from 'postcss'
import autoprefixer from 'autoprefixer'
// rollup.config.js
export default {
  input: `src/${process.env.FILENAME}.js`,
  output: {
    file: `dist/${process.env.FILENAME}.min.js`,
    format: 'esm',
    name: 'cc-custom-style'
  },
  plugins: [scss({
    processor: () => postcss([autoprefixer({ overrideBrowserslist: 'last 2 versions' })])
  })]
};